#include "Connect64Window.h"

#include "../Utils/Utils.h"

#define DIAGNOSTIC_OUTPUT

namespace view {

//
// Constructs a connect 64 puzzle window creating and initializing all the widgets that will be displayed
//
// @precondition  width > 0 AND height > 0
// @postcondition  none
//
// @param width  The width of the window
// @param height  The height of the window
// @param title  The title of the window
//
Connect64Window::Connect64Window(int width, int height, const char* title) : Fl_Window(width, height, title)
{
    begin();

    this->createPuzzleChoice();
    this->createAndDisplayPuzzle();

    this->num = this->getNextNumber();
    this->puzzleButtonLabel = new Fl_Input(100, 200, 70, 25, "Number:");
    this->puzzleButtonLabel->value("1");

    this->validateButton = new Fl_Button(570, 580, 70, 30, "Validate");
    this->validateButton->callback(cbValidate, this);

    this->messageLabel = new Fl_Output(205, 300, 0, 0, "");

    this->resetButton = new Fl_Button(100, 350, 110, 30, "Reset Board");
    this->resetButton->callback(cbResetPuzzle, this);

    end();
}

void Connect64Window::createPuzzleChoice()
{
    this->puzzledropdown = new Fl_Choice(120, 25, 70, 20, "Puzzles");
    for (int i = 1; i <= 4; i++) {
        string puzzle = to_string(i);
        this->puzzledropdown->add(puzzle.c_str());
    }
    this->puzzledropdown->callback(cbPlayPuzzle, this);
    this->puzzledropdown->value(0);
}


void Connect64Window::createAndDisplayPuzzle()
{
    static const int X = 400;
    static const int Y = 160;

    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLUMNS; j++)
        {
            int offset = (i*COLUMNS)+j;
            int value = this->controller.getPuzzle(to_string(this->puzzledropdown->value())).atPoint(j,i);
            string input = to_string(value);
            string* myval = new string(input);

            if(value != 0)
            {
                this->puzzle_values.push_back(value);
                this->puzzle[offset] = new Fl_Button(X + i*50, Y + j*50, 50, 50, myval->c_str());
                this->puzzle[offset]->deactivate();

#ifdef DIAGNOSTIC_OUTPUT
    cout << "Input: " << myval->c_str() << ", at row: " << j << ", column: " << i << endl;
#endif // DIAGNOSTIC_OUTPUT

            } else {
                this->puzzle[offset] = new Fl_Button(X + i*50, Y + j*50, 50, 50, "");
            }

            this->puzzle[offset]->callback(cbIncrementNumber, this);
            this->displayedPuzzle.atPoint(j, i) = value;
        }
    }
}

//
// Callback when puzzle's current number has changed
//
// @precondition  widget != 0 AND data != 0
// @postcondition  Connect64Window::currentNumber ++ or puzzleButtonLabel
//
// @param widget  The widget that initiated the callback
// @param data  Any data that was passed with the call back. In this instance, a pointer to the window
//
void Connect64Window::cbIncrementNumber(Fl_Widget* widget, void* data)
{
    Fl_Button* button = (Fl_Button*)widget;
    Connect64Window* window = (Connect64Window*)data;

    window->num++;
    string s = to_string(window->num);
    const char* ptr = s.c_str();
    window->puzzleButtonLabel->value(ptr);

    button->label(window->puzzleButtonLabel->value());
}

void Connect64Window::cbPlayPuzzle(Fl_Widget* widget, void* data)
{
    Connect64Window* window = (Connect64Window*)data;

    switch(window->puzzledropdown->value())
    {
    case 0: window->changeButtonLabels(0);break;
    case 1: window->changeButtonLabels(1);break;
    case 2: window->changeButtonLabels(2);break;
    case 3: window->changeButtonLabels(3);break;
    }
}

void Connect64Window::cbResetPuzzle(Fl_Widget* widget, void* data)
{
    Connect64Window* window = (Connect64Window*)data;
    int currentpuzzle = window->puzzledropdown->value();
    window->changeButtonLabels(currentpuzzle);
    window->puzzleButtonLabel->value("1");
}


void Connect64Window::changeButtonLabels(int choice)
{
    this->clearAllButtonLabels();

    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLUMNS; j++)
        {
            int offset = (i*COLUMNS)+j;
            int value = this->controller.getPuzzle(to_string(choice)).atPoint(j,i);
            string input = to_string(value);
            string* myval = new string(input);

            if(value != 0)
            {
                this->puzzle_values.push_back(value);
                this->puzzle[offset]->label(myval->c_str());
                this->puzzle[offset]->deactivate();
#ifdef DIAGNOSTIC_OUTPUT
    cout << "Input: " << input.c_str() << ", at row: " << j << ", column: " << i << endl;
#endif // DIAGNOSTIC_OUTPUT
            } else {
                this->puzzle[offset]->label("");
            }
            this->displayedPuzzle.atPoint(j, i) = value;
        }
    }
}

void Connect64Window::clearAllButtonLabels()
{
    for (int i = 0; i < ROWS; i++)
    {
        for (int j = 0; j < COLUMNS; j++)
        {
            int offset = (i*COLUMNS)+j;
            this->puzzle[offset]->activate();
            this->puzzle[offset]->label("");
        }
    }
    this->puzzle_values = {};
}

//
// Callback that is an instance of the window class to encapsulate function
//
// @precondition  none
// @postcondition  Connect64Window::num++
//
void Connect64Window::updateNumber()
{
    if (std::find(this->puzzle_values.begin(), this->puzzle_values.end(), this->getNextNumber()) != this->puzzle_values.end()) {
        ++this->num;
        this->updateNumber();
    }

    this->setNextNumber();
}

//
// Callback when the Validate button is invoked
//
// @precondition  widget != 0 AND data != 0
// @postcondition  Connect64Window::currentNumber ++
//
// @param widget  The widget that initiated the callback
// @param data  Any data that was passed with the call back. In this instance, a pointer to the button clicked
//
void Connect64Window::cbValidate(Fl_Widget* widget, void* data)
{
    Connect64Window* window = (Connect64Window*)data;
    Puzzle displayed = window->getDisplayedPuzzle();
    string message = validatePuzzle(displayed);
    window->messageLabel->label(message.c_str());
}

//
// Gets the current displayed puzzle
//
// @precondition  none
// @postcondition  none
//
// @return  The puzzle that is currently displayed
//
Puzzle Connect64Window::getDisplayedPuzzle()
{
    #ifdef DIAGNOSTIC_OUTPUT
cout << "\nDisplayed Puzzle" << endl;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLUMNS; j++) {
            if(j == COLUMNS-1) {
                cout << this->displayedPuzzle.atPoint(i,j);
            } else {
                cout << this->displayedPuzzle.atPoint(i,j) << " , ";
            }
        }
        cout << endl;
    }
#endif // DIAGNOSTIC_OUTPUT

    return this->displayedPuzzle;
}

//
// Determines and sets the next number based on the displayed puzzle
//
// @precondition  none
// @postcondition  Connect64Window::num == next needed value
//
void Connect64Window::setNextNumber()
{
    this->num = this->getNextNumber();
}

//
// Determines the next number based on the displayed puzzle
//
// @precondition  none
// @postcondition  none
//
// @return  The next number needed in the puzzle
//
int Connect64Window::getNextNumber()
{
    vector<int> values = this->puzzle_values;
    sort(values.begin(), values.end());
    int next = this->num;
    for(vector<int>::iterator it = values.begin(); it != values.end(); ++it) {
        if (*it != next) {
            return next;
        } else {
            ++next;
        }
    }

    return next;
}

//
// Destructor that cleans up all the allocated resources for the Connect64Window object
//
Connect64Window::~Connect64Window()
{
    delete this->messageLabel;
    delete this->puzzleButtonLabel;
    delete this->validateButton;
    delete this->deleteButton;
    delete this->resetButton;
    for (int j = 0; j < 64; j++) {
            delete this->puzzle[j];
    }
}

}
