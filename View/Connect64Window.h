#ifndef CONNECT64WINDOW_H
#define CONNECT64WINDOW_H

#include <FL/Fl_Window.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

#include "../Controller/GameController.h"
#include "../Model/Puzzle.h"
#include "../Utils/Utils.h"
#include "../Utils/Validate.h"

using namespace model;
using namespace controller;

using namespace std;

namespace view {

class Connect64Window : public Fl_Window
{
    enum PuzzleChoice {ONE, TWO, THREE, FOUR};

    public:
        Connect64Window(int width, int height, const char* title);

        static void cbIncrementNumber(Fl_Widget* widget, void* data);
        static void cbPlayPuzzle(Fl_Widget* widget, void* data);
        static void cbResetPuzzle(Fl_Widget* widget, void* data);
        static void cbUpdateNumber(Fl_Widget* widget, void* data);
        void updateNumber();

        static void cbValidate(Fl_Widget* widget, void* data);

        virtual ~Connect64Window();

    private:
        int count = 1;
        int num = 0;

        static const int STARTING_PUZZLES = 4;
        static const int ROWS = Puzzle::PUZZLE_SIZE;
        static const int COLUMNS = Puzzle::PUZZLE_SIZE;

        vector<int> puzzle_values = {};
        vector<string> puzzle_types = {"1", "2", "3", "4"};
        string currentPuzzle_type = "1";
        Puzzle displayedPuzzle;
        GameController controller;

        Fl_Output * messageLabel;
        Fl_Input * puzzleButtonLabel;
        Fl_Choice* puzzledropdown;
        Fl_Button * puzzle [64] = {};
        Fl_Button * deleteButton;
        Fl_Button * resetButton;
        Fl_Button * validateButton;

        void createAndDisplayPuzzle();
        void createPuzzleChoice();
        void setPuzzleBasedOnSelection();
        void changeButtonLabels(int value);
        void clearAllButtonLabels();

        Puzzle getCurrentPuzzle();
        Puzzle getDisplayedPuzzle();
        void setNextNumber();
        int getNextNumber();
};

}

#endif // CONNECT64WINDOW_H
