#ifndef CSVREADER_H
#define CSVREADER_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

vector<vector<string> > getFileData(const string& filename);
#endif // CSVREADER_H
