#ifndef VALIDATE_H
#define VALIDATE_H

#include "../Model/Puzzle.h"

#include <iostream>

using namespace std;

using namespace model;

const string validatePuzzle(Puzzle aPuzzle);

#endif // VALIDATE_H
