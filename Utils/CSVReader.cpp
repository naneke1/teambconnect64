#include "CSVReader.h"

#define DIAGNOSTIC_OUTPUT

//
// Reads in a file name and retrieves the data from it
//
// @precondition  none
// @postcondition  none
//
// @param filename  the name of the file to pull data from
//
// @return vector data of the csv file
//
vector<vector<string> > getFileData(const string& filename)
{
    ifstream file (filename.c_str());
    vector<vector<string> > datalist;
    string line = "";

    if (file)
    {
        while (getline(file, line))
        {
            stringstream sep(line);
            string data;
            datalist.push_back(vector<string>());

            while (getline(sep, data, ','))
            {
                datalist.back().push_back(data);
            }
        }
    }
#ifdef DIAGNOSTIC_OUTPUT
    for (vector<vector<string> >::size_type row = 0; row < datalist.size(); row++)
    {
        for (vector<string>::size_type column = 0; column < datalist[row].size(); column++)
        {
            if(column == datalist[row].size()-1){
                cout << datalist[row][column];
            } else {
                cout << datalist[row][column] << " , ";
            }
        }
        cout << endl;
    }
#endif // DIAGNOSTIC_OUTPUT

    return datalist;
}
