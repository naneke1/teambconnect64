#ifndef UTILS_H
#define UTILS_H

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int convertValue(const string& value);

int convertValue(const char* value);

#endif // UTILS_H
