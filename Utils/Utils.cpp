#include "Utils.h"

#define DIAGNOSTIC_OUTPUT

//
// Converts a string to an int
//
// @precondition  none
// @postcondition  none
//
// @param value  The string to convert to an int
//
// @return int of string value
//
int convertValue(const string& value)
{
    if(value == "") return 0;

    int intValue;
    if(!(stringstream(value) >> intValue)) intValue = 0;

#ifdef DIAGNOSTIC_OUTPUT
    cout << value << " to " << intValue  << endl;
#endif // DIAGNOSTIC_OUTPUT

    return intValue;
}

//
// Converts a char* to an int
//
// @precondition  none
// @postcondition  none
//
// @param value  The char* to convert to an int
//
// @return int of char* value
//
int convertValue(const char* value)
{
    if(value[0] == '\0') return 0;

    int intValue;
    if(!(stringstream(value) >> intValue)) intValue = 0;

#ifdef DIAGNOSTIC_OUTPUT
    cout << "\n" << value << " to " << intValue << endl;
#endif // DIAGNOSTIC_OUTPUT

    return intValue;
}
