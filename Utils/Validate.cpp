#include "Validate.h"

#define DIAGNOSTIC_OUTPUT

//
// Reads in a puzzle and determines if it is valid
//
// @precondition  none
// @postcondition  none
//
// @param aPuzzle  The puzzle to check if valid
//
// @return validation message describing if valid or not
//
const string validatePuzzle(Puzzle aPuzzle)
{
    string message = "Puzzle is valid        ";
    Point currPoint;
    int currValue = 1;
    int puzzle [Puzzle::PUZZLE_SIZE][Puzzle::PUZZLE_SIZE];

    currPoint = aPuzzle.findValue(currValue);
    ++currValue;

    while (currValue < 65) {
    bool hasNext = false;

        //checks above current point
        if (currPoint.row-1 >= 0) {
            if (aPuzzle.atPoint(currPoint.row-1, currPoint.column) == currValue) {
                currPoint = aPuzzle.findValue(currValue);
                hasNext = true;
                ++currValue;
            }
        }

        //checks below current point
        if (currPoint.row+1 < Puzzle::PUZZLE_SIZE) {
            if (aPuzzle.atPoint(currPoint.row+1, currPoint.column) == currValue) {
                currPoint = aPuzzle.findValue(currValue);
                hasNext = true;
                ++currValue;
            }
        }

        //checks left of current point
        if (currPoint.column-1 >= 0) {
            if (aPuzzle.atPoint(currPoint.row, currPoint.column-1) == currValue) {
                currPoint = aPuzzle.findValue(currValue);
                hasNext = true;
                ++currValue;
            }
        }

        //checks right of current point
        if (currPoint.column+1 < Puzzle::PUZZLE_SIZE) {
            if (aPuzzle.atPoint(currPoint.row, currPoint.column+1) == currValue) {
                currPoint = aPuzzle.findValue(currValue);
                hasNext = true;
                ++currValue;
            }
        }

        if (!hasNext) {
            message = to_string(currValue) + " was not correct";
            return message;
        }

        hasNext = false;
    }

    if (currPoint.row == -1 || currPoint.column == -1) {
        message = "Puzzle is invalid";
    }

    return message;
}
