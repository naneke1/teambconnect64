#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>

#include "../View/Connect64Window.h"

using namespace view;

///
/// Runs the Connect64 program.
///
int main (int argc, char ** argv)
{
  Connect64Window mainWindow(1000, 700, "Connect 64 by Nnenna Aneke & Ariel Scott");
  mainWindow.show();

  int exitCode = Fl::run();
  return exitCode;
}
