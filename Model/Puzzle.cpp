#include "Puzzle.h"

#include "../Utils/Utils.h"

#define DIAGNOSTIC_OUTPUT

namespace model {

//
// Constructs a puzzle object
//
// @precondition  none
// @postcondition  none
//
Puzzle::Puzzle() : columns(PUZZLE_SIZE, Column(PUZZLE_SIZE))
{
    this->puzzle.resize(PUZZLE_SIZE);
    for(int i = 0; i < PUZZLE_SIZE; i++){
        this->puzzle[i].resize(PUZZLE_SIZE);
    }

    for (int row = 0; row < PUZZLE_SIZE; row++) {
        for (int column = 0; column < PUZZLE_SIZE; column++) {
            columns[row][column] = 0;
            #ifdef DIAGNOSTIC_OUTPUT
                cout << " " << columns[row][column];
            #endif // DIAGNOSTIC_OUTPUT
        }
        cout << " Created empty puzzle" << endl;
    }
}

//
// Constructs a puzzle object
//
// @precondition  none
// @postcondition  none
//
// @param inputPuzzle  puzzle to use for the class
//
Puzzle::Puzzle(vector<vector<string> > inputPuzzle) : columns(PUZZLE_SIZE, Column(PUZZLE_SIZE))
{
    this->puzzle.resize(PUZZLE_SIZE);
    for(int i = 0; i < PUZZLE_SIZE; i++){
//        this->puzzle[i].resize(PUZZLE_SIZE);
    }

    for (int row = 0; row < PUZZLE_SIZE; row++) {
        for (int column = 0; column < PUZZLE_SIZE; column++) {

            string input = inputPuzzle[row][column];
            int value = convertValue(input);

            columns[row][column] = value;
            #ifdef DIAGNOSTIC_OUTPUT
                cout << " " << columns[row][column] ;
            #endif // DIAGNOSTIC_OUTPUT
        }
        cout << endl;
    }
}

//
// Gets/sets the value at the point given
//
// @precondition  none
// @postcondition  if set: columns[row][column] = value
//
// @return  if get: The value at columns[row][column]
//
int& Puzzle::atPoint(int row, int column)
{
    return columns[row][column];
}

//
// Gets the location of a value
//
// @precondition  none
// @postcondition  Point = (-1,-1) if value not found
//
// @return  Point with row and column of value
//
Point Puzzle::findValue(int value)
{
    Point point;
    for (int row = 0; row < PUZZLE_SIZE; row++) {
        for (int column = 0; column < PUZZLE_SIZE; column++) {
            int puzzleVal = this->atPoint(row, column);
            if(puzzleVal == value)
            {
                point.row = row;
                point.column = column;
            }
        }
    }
#ifdef DIAGNOSTIC_OUTPUT
    cout << "\nFound " << value << " at " << point.row << ", " << point.column << endl;
#endif // DIAGNOSTIC_OUTPUT
    return point;
}


//
// Destructor that cleans up all the allocated resources for the Puzzle object
//
Puzzle::~Puzzle()
{
    this->puzzle.clear();
}

}
