#ifndef PUZZLE_H
#define PUZZLE_H

#include <vector>
#include <string>
using namespace std;

namespace model {

//
// Defines a Point struct
//
struct Point {
    int column;
    int row;
    Point()
    {
        row = -1;
        column = -1;
    }
    Point(int row, int column)
    {
        row = row;
        column = column;
    }
};

//
// Defines a puzzle
//
class Puzzle
{
    public:
        static const int PUZZLE_SIZE = 8;
        typedef vector<int> Column;
        vector<Column> columns;

    private:
        vector<vector<int> > puzzle{PUZZLE_SIZE, vector<int>(PUZZLE_SIZE, 0)};

    public:
        Puzzle();
        Puzzle(vector<vector<string> > inputPuzzle);
        //Puzzle(vector<vector<string> > inputPuzzle);
        //Puzzle();
        virtual ~Puzzle();

        int& atPoint(int row, int column);
        Point findValue(int value);
};

}

#endif // PUZZLE_H
