#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <string>
#include <vector>
#include "../Model/Puzzle.h"

using namespace std;
using namespace model;

namespace controller {

//
// Defines a game controller
//
class GameController
{
    public:
        GameController();
        virtual ~GameController();

        Puzzle getPuzzle(const string& puzzle_type);
        string getPuzzleInPlay();

    private:
        vector<Puzzle*> puzzles;
        string puzzleInPlay;
};

}

#endif // GAMECONTROLLER_H
